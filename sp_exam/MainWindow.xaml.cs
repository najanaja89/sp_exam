﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sp_exam
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();


        }

        private void ButtonNumberGoClick(object sender, RoutedEventArgs e)
        {
            if (Regex.IsMatch(textBoxNumbers.Text, @"^[1-9]\d*$")) 
            {
                int number = (int.Parse(textBoxNumbers.Text));
                int[] intArray = Enumerable.Range(0, number).ToArray();

                //Parallel.For(0, number, (i) =>
                //{
                //    intArray[i] = i;
                //});

                //foreach (var n in intArray)
                //{
                //    MessageBox.Show(n.ToString());
                //}

                Task.Run(() =>
                    {
                        foreach (var n in intArray)
                        {
                            intArray[n] = n;
                        }
                    });

                foreach (var n in intArray)
                {
                    MessageBox.Show(n.ToString());
                }
            }
            else
            {
                MessageBox.Show("Invalid number! Number must be > 0");
            }
        }

        private void WriteButtonClick(object sender, RoutedEventArgs e)
        {
            using (var context= new CharacterContext())
            {
                var character = new Character();
                character.FirstName=firstNameTextBox.Text;
                character.LastName = lastNameTextBox.Text;
                character.Alias = aliasTextBox.Text;
                character.SourceUniverse = universeTextBox.Text;

                if (isAliveCheckBox.IsChecked==true)
                {
                    character.IsAlive = true;
                }
                else character.IsAlive = false;
                context.characters.Add(character);
                
                context.SaveChangesAsync();
            }
        }
    }
}
