﻿namespace sp_exam
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class CharacterContext : DbContext
    {
        // Контекст настроен для использования строки подключения "CharacterContext" из файла конфигурации  
        // приложения (App.config или Web.config). По умолчанию эта строка подключения указывает на базу данных 
        // "sp_exam.CharacterContext" в экземпляре LocalDb. 
        // 
        // Если требуется выбрать другую базу данных или поставщик базы данных, измените строку подключения "CharacterContext" 
        // в файле конфигурации приложения.
        public CharacterContext()
            : base("name=CharacterContext")
        {
        }
        public DbSet<Character> characters { get; set; }

    // Добавьте DbSet для каждого типа сущности, который требуется включить в модель. Дополнительные сведения 
    // о настройке и использовании модели Code First см. в статье http://go.microsoft.com/fwlink/?LinkId=390109.

    // public virtual DbSet<MyEntity> MyEntities { get; set; }
}

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}